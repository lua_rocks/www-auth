-- 22-01-2024 @author Swarg
--
-- Goal: authentication through `www-authenticate: Bearer realm=...`
-- first use: auth.docker.io
--
-- Dependencies:
--
--   Bearer Auth:
--     - alogger, luasocket, cjson (installed via luarocks)
--
local M = {}

M._VERSION = 'www-auth 0.1.0'
M._URL = 'https://gitlab.com/lua_rocks/www-auth'

local log = require('alogger')

---@diagnostic disable-next-line unused-local
local inspect = require("inspect")

-- local DATE_FORMAT = '%Y-%m-%d %H:%M:%S'
-- directory to store tokens with expiration date
local AUTHCACHE_DIR = '.cache/www-auth'
local use_cache = true

local E = {}

--------------------------------------------------------------------------------
-- Work with files
--------------------------------------------------------------------------------

--
---@param dir1 string?
---@param dir2 string?
---@param file string?
local function join_path(dir1, dir2, file)
  local path = log.join_path(dir1, dir2)
  if file and file ~= '' then
    path = log.join_path(path, file)
  end
  return path
end

local function file_exists(fn)
  return log.file_exists(fn)
end

local function read_all_bytes_from(fn)
  local file = io.open(fn, "rb")   -- r read mode and b binary mode
  if file then
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
  end
  return nil
end

-- Open File for given mode with error handling
-- if need silent mode with no error set throw_error = "no"
---@param filename string
---@param mode string rb, w, a
---@param throw_error any|nil
function M.open_file(filename, mode, throw_error)
  if filename then
    if not mode then mode = "r" end
    local f, err = io.open(filename, mode)
    if f then
      return f
    else
      if not throw_error or throw_error ~= "no" then
        error("Error opening file: " .. err)
      end
    end
  end
  if not throw_error or throw_error ~= "no" then
    error("No FileName to Open")
  end
  return nil
end

-- write text to given filename
-- if append - add to the end of file or rewrite last content
---@param filename string
---@param text string
---@param mode string|nil Defaul "w" -- ReWrite
local function write2file(filename, text, mode)
  text = text or ""
  mode = mode or "w"                       -- rewrite, use a to append
  local file = M.open_file(filename, mode) -- mode "w" | "a"
  if file then
    file:write(text)
    file:close()
    return true
  end
  return false
end

--------------------------------------------------------------------------------

-- directory-storage to cached tockens
function M.get_authcache_dir()
  local dir = AUTHCACHE_DIR or '.auth'
  if (dir):sub(1, 1) == '/' then
    return AUTHCACHE_DIR
  end
  return join_path(os.getenv('HOME'), dir)
end

-- for testing
function M.set_auth_cache_dir(dir)
  AUTHCACHE_DIR = dir or '.cache/www-auth'
end

local function mk_flat_filename(s)
  if type(s) == 'string' then
    return s:gsub(' ', '_'):gsub('/', '_'):gsub(':', '_'):gsub('\\', '_')
  end
  return s
end

function M.get_full_path_in_authcache(realm, relpath)
  realm = realm and mk_flat_filename(realm) or 'unknown'

  local fn = join_path(M.get_authcache_dir(), realm, relpath)
  return fn
end

--
-- entrypoint called on `401 Unauthorized` message
-- attempt to apply supported authenticate
--
---@param reqt table {headers}  -- add auth headers into whis table
---@param resp_headers table    -- headers with 401 answer from server
---@param resp_body table
---@param ui_callback function  -- to ask user:password via ui
function M.auth(reqt, resp_headers, resp_body, ui_callback)
  local header = resp_headers['www-authenticate']
  if header then
    local hauth = M.parse_auth_header(header)
    if hauth.name == 'Basic' then
      return M.apply_basic_auth(reqt, ui_callback)
      --
    elseif hauth.name == 'Bearer' then
      return M.apply_bearer_auth(reqt, hauth, resp_headers, resp_body)
    end
  end
  return false
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--
-- parse http header with www-authenticate
-- www-authenticate: Basic realm="Closed Site Area"
-- Bearer realm="https://auth.host.io/token",service="registry.host.io",scope="repository:library/alpine:pull"
---@param header string
---@return table {name, realm, ...}
function M.parse_auth_header(header)
  local name, props = string.match(header, "^([^%s]+)%s+(.*)%s*$")
  local t = { name = name, realm = nil }

  if props and props ~= '' then
    for kv in string.gmatch(props, '([^,]+)') do
      local k, v = string.match(kv, '^([%w]+)="([^"]+)"')
      if k and v then
        t[k] = v
      end
    end
  end

  return t
end

-- "https://auth.host.io/token" --> https, auth.host.io, token
function M.parse_realm_url(s)
  if s then
    local query = nil
    local scheme, host, path = string.match(s, '^([%w]+)%://([^/]+)(.*)$')
    return { scheme = scheme, host = host, path = path, query = query }
  end
  return s
end

--------------------------------------------------------------------------------
--                               BASIC
--------------------------------------------------------------------------------

--
-- www-authenticate: Basic
--
-- simple interactive mode, then the user must enter authentication data
--
---@param reqt table
---@param ui_callback function
function M.apply_basic_auth(reqt, ui_callback)
  assert(type(ui_callback) == 'function', 'ui_callback')

  local user = ui_callback('USERNAME:', 'user')
  local pass = ui_callback('PASSWORD:', 'passwd')

  local mime = require("mime")
  local url = require("socket.url")

  if user and user ~= '' and pass and pass ~= '' then
    reqt.headers["authorization"] =
        "Basic " .. (mime.b64(user .. ":" .. url.unescape(pass)))
    return true
  else
    reqt.err = 'user and password not provided'
    return false
  end
end

--------------------------------------------------------------------------------
--                              BEARER
--------------------------------------------------------------------------------

-- update hauth {name, realm}
---@param hauth table
function M.parse_bearer_data(hauth)
  local turl = M.parse_realm_url(hauth.realm)
  hauth.realm_url = turl
  if not hauth.realm_url then
    log.debug('Error on parse bearer auth data: no realm_url for', hauth.realm)
    return nil
  end
  return hauth
end

-- www-authenticate: Bearer
---@param reqt table
---@param hauth table {name, realm, ...}  -- props from www-authenticate
---@param resp_body table{string}
function M.apply_bearer_auth(reqt, hauth, resp_headers, resp_body)
  if not M.parse_bearer_data(hauth) then
    return false
  end

  local ok, token = M.get_bearer_token(reqt, hauth, resp_headers, resp_body)
  if not ok or not token or token == '' then
    log.debug('cannot get bearer token')
    return false
  end

  -- pick token and into reqt.headers.authenticate
  reqt.headers.authorization = 'Bearer ' .. tostring(token)

  return true
end

--
-- from bearer scope build path in cache
--  - check token from cache (is alive)
--  - if expired or not exist get new auth token
--
---@param reqt table
---@param hauth table{name, realm,...[service,scope]}
---@param resp_headers table{date}
---@param resp_body table{strings}
---@return boolean success
---@return string? (token)
function M.get_bearer_token(reqt, hauth, resp_headers, resp_body)
  local json_str
  local scope_path = M.bearer_scope2path(hauth, resp_body)

  log.debug("get_bearer_token", ((hauth or E).realm_url or E).host, scope_path)
  ---@diagnostic disable-next-line: param-type-mismatch
  local now = M.parse_data_header(resp_headers.date) or os.time(os.date("!*t"))
  if use_cache then
    json_str = M.get_cached_bearer_json(hauth.realm_url.host, scope_path, now)
  end

  if not json_str then
    local ok
    ok, json_str = M.get_new_bearer_token(reqt, hauth)
    if ok and json_str and use_cache then
      M.save_bearer_json(hauth.realm_url.host, scope_path, json_str)
    else
      log.debug('ok:', ok, type(json_str), use_cache)
    end
  end

  local cjson = require("cjson")

  local ok, decoded = pcall(cjson.decode, json_str)
  if not ok then
    local err_msg = decoded
    reqt.err = 'cjson.decode err: "' .. tostring(err_msg) .. '"'
    log.debug('cannot parse json responce: %s', err_msg)
    return false
  end

  return true, decoded.token
end

--
-- realm="https://auth.host.io/token", -- parser into turl
-- service="registry.host.io",
-- scope="repository:host/alpine:pull"
--
---@param hauth table {name, realm, realm_url, ...} -- from www-authenticate
---       realm_url is table { scheme, host, path, query}
---@return string uri
function M.build_bearer_uri(hauth)
  assert(type(hauth.realm_url) == 'table', 'realm_url must be already parsed')

  local url = require("socket.url")
  local path = hauth.realm_url.path or '/'
  local i = 0

  -- build query string to uri
  for k, v in pairs(hauth) do
    if k ~= 'realm' and k ~= 'name' and k ~= 'realm_url' then
      if i == 0 then
        path = path .. '?'
      elseif i > 0 then
        path = path .. '&'
      end
      path = path .. k .. '=' .. url.escape(v)
      i = i + 1
    end
  end

  return path
end

-- tmp key storage in file
---@param hauth table
---@param resp_body table
function M.bearer_scope2path(hauth, resp_body)
  if hauth and hauth.scope then
    return mk_flat_filename(hauth.scope)
  elseif resp_body then
    --  TODO
    -- {"errors":[
    --  {"code":"UNAUTHORIZED",
    --   "message":"authentication required",
    --   "detail":[
    --  {"Type":"repository","Class":"","Name":"library/alpine","Action":"pull"}
    -- ]
    -- }]}
  end
end

-- 2024-01-22T16:20:51.86663282Z" --> epoch sec
---@param date string
---@return number
function M.parse_string_data(date)
  local pattern = "^(%d+)%-(%d+)%-(%d+)T(%d+):(%d+):(%d+)%.(%d+)Z"
  local y, m, d, h, min, s, _ = date:match(pattern)
  local timestamp = os.time({
    year = y, month = m, day = d, hour = h, min = min, sec = s
  })

  return timestamp
end

local MONTHS = {
  Jan = 1,
  Feb = 2,
  Mar = 3,
  Apr = 4,
  May = 5,
  Jun = 6,
  Jul = 7,
  Aug = 8,
  Sep = 9,
  Oct = 10,
  Nov = 11,
  Dec = 12
}

local function get_gmt_offset()
  ---@diagnostic disable-next-line: param-type-mismatch
  local offset = os.time() - os.time(os.date("!*t"))
  -- local gmt = os.date("!*t") local loc = os.date("*t")
  return offset
end

local TIME_ZONE_OFFSET = get_gmt_offset()

-- date: Mon, 22 Jan 2024 17:14:44 GMT
---@param data string
---@return number
function M.parse_data_header(data, tolocal)
  local p = "^%a+, (%d+) (%a+) (%d+) (%d+):(%d+):(%d+) GMT$"
  local d, m, y, h, min, sec = data:match(p)
  m = MONTHS[m]
  local epoch = os.time({
    day = d,
    month = m,
    year = y,
    hour = h,
    min = min,
    sec = sec
  })
  if tolocal then
    epoch = TIME_ZONE_OFFSET
  end
  return epoch
end

--[[{
  "token": "eyJhbGx...BDkfqPOU3QnQvYHP1Cmr1k_UH05oUde2i4XlTU27oAnA",
  "access_token": "eyJhbGci..DkfqPOU3QnQvYHP1Cmr1k_UH05oUde2i4XlTU27oAnA",
  "expires_in": 300, -- seconds
  "issued_at": "2024-01-22T11:10:32.593164076Z"
}]]
---@param json_str string
---@param now number os.time()
function M.is_bearer_token_expired(json_str, now)
  local cjson = require("cjson")

  local ok, j = pcall(cjson.decode, json_str)
  if ok and j.issued_at and j.expires_in then
    local issued_at = M.parse_string_data(j.issued_at)

    local expired_at = (issued_at + j.expires_in)
    local alive_time = expired_at - now
    if log.is_debug() then
      log.debug('alive_time sec:', alive_time)
      -- local fmt = DATE_FORMAT
      -- log.debug('now:%s, issued_at: %s, ttl min:%s', os.date(fmt, now),
      --   os.date(fmt, issued_at), j.expires_in / 60)
    end
    return alive_time < 0
  else
    log.debug("is_bearer_token_expired ", j.issued_at, j.expires_in)
  end
  return false
end

--
---@param realm string?
---@param relpath string
---@param remove_expired boolean?  by default do not delete expired json data
---@return string?
function M.get_cached_bearer_json(realm, relpath, now, remove_expired)
  local fn = M.get_full_path_in_authcache(realm, relpath)
  if file_exists(fn) then
    local json = read_all_bytes_from(fn)
    if not json then
      log.debug('cannot read ', fn)
      return nil
      --
    elseif M.is_bearer_token_expired(json, now) then
      log.debug("bearer_token is expired")
      if remove_expired then
        os.remove(fn) -- delete file
        log.debug("expired data is deleted", fn)
      end
      return nil
    else
      log.debug('bearer token taken from cache')
    end
    return json
  else
    log.debug('BearerToken: file not found', fn)
  end
  return nil
end

-- save json into auth cache to reuse
---@param realm string?
---@param relpath string  relative filename for save token in auth cache dir
---@param json_str string
function M.save_bearer_json(realm, relpath, json_str)
  assert(type(relpath) == 'string' and relpath ~= '', 'in cache filename')
  assert(type(json_str) == 'string' and json_str ~= '', 'json_str')

  local fn = M.get_full_path_in_authcache(realm, relpath)

  local dir = string.match(fn, "(.*[/\\])") -- or path
  log.ensure_dir_exists(dir)
  -- if not dir_exists(dir) then
  --   if not mkdir(dir) then
  --     print('[WARN] Cannot create dir "' .. tostring(dir) .. '" to save token')
  --   end
  --   return nil
  -- end

  local saved = write2file(fn, json_str)

  log.debug('save_bearer_json', dir, fn, #json_str, saved)
  return saved
end

---@param reqt table
---@param hauth table
---@return boolean success staus
---@return string? json
function M.get_new_bearer_token(reqt, hauth)
  log.debug("get_new_bearer_token")
  local realm_url = (hauth or E).realm_url

  if not realm_url or not realm_url.host or not realm_url.scheme then
    reqt.err = 'Not Found host or scheme in auth realm_url'
    log.debug('%s: %s', reqt.err, realm_url)
    return false
  end

  local http = require("socket.http")
  local ltn12 = require("ltn12")
  local response_body = {}

  local new_reqt = {
    scheme = realm_url.scheme, -- https | http
    method = 'GET',
    uri = M.build_bearer_uri(hauth),
    host = realm_url.host,
    headers = { accept = 'application/json' }, -- ?
    proxy = reqt.proxy,
    sink = ltn12.sink.table(response_body),
  }
  local r, code, headers, status = http.request(new_reqt)
  if not r then
    reqt.err = 'http.req err:' .. tostring(code)
    log.debug(reqt.err)
    return false
  end

  if code ~= 200 then
    print('[DEBUG]', inspect(new_reqt))
    reqt.err = status
    log.debug('response code is', code, status)
    return false
  end

  -- parse json response
  local content_type = (headers or E)['content-type']
  if not content_type or not string.find(content_type, "application/json") then
    reqt.err = 'Not Supported Content-Type: ' .. tostring(content_type)
    log.debug(reqt.err)
    return false
  end

  local json_str = table.concat(response_body, ' ')

  log.debug("get_new_bearer_token json len:", #json_str)

  return true, json_str
end

return M
