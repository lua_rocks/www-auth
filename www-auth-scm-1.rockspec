---@diagnostic disable: lowercase-global
package = "www-auth"
version = "scm-1"
source = {
   url = "git+https://gitlab.com/lua_rocks/www-auth"
}
description = {
   homepage = "https://gitlab.com/lua_rocks/www-auth",
   license = "MIT"
}
dependencies = {
  "lua >= 5.1",
  "alogger >= 0.2",
}
build = {
   type = "builtin",
   modules = {
      ["www-auth"] = "src/www-auth.lua"
   }
}
