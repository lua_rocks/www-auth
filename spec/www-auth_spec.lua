-- 22-01-2024 @author Swarg
require("busted.runner")()
local assert = require("luassert")

local M = require("www-auth");

---@diagnostic disable-next-line unused-local
local log = require('alogger')

local uv = require("luv")
-- local inspect = require("inspect")



describe("env.bridges.www-auth", function()
  it("parse_auth_header", function()
    local f = M.parse_auth_header
    local exp = { name = 'Basic', realm = 'Closed Site Area' }
    assert.same(exp, f('Basic realm="Closed Site Area"'))

    local header = 'Bearer realm="https://auth.host.io/token",' ..
        'service="registry.host.io",scope="repository:library/alpine:pull"'
    local exp2 = {
      name = 'Bearer',
      realm = 'https://auth.host.io/token',
      service = 'registry.host.io',
      scope = 'repository:library/alpine:pull',
    }
    assert.same(exp2, f(header))
  end)

  it("parse_realm", function()
    local f = function(realm)
      local t = M.parse_realm_url(realm)
      return tostring(t.scheme) .. '|' .. tostring(t.host) .. '|' .. tostring(t.path)
    end
    assert.same('https|auth.host.io|/token', f("https://auth.host.io/token"))
    assert.same('https|auth.my-host.io|/tkn', f("https://auth.my-host.io/tkn"))
    assert.same('http|auth.my_host.io|/tkn', f("http://auth.my_host.io/tkn"))
    assert.same('http|auth.my_host.io|/', f("http://auth.my_host.io/"))
    assert.same('http|auth.my_host.io|', f("http://auth.my_host.io"))
  end)

  it("build_bearer_uri", function()
    local f = M.build_bearer_uri

    local h_auth = { -- from M.parse_auth_header
      name = 'Bearer',
      realm = 'https://auth.hst.io/tkn',
      service = 'reg.hst.io',
      scope = 'repo:library/alpine:pull',
    }
    local exp = '/tkn?scope=repo%3alibrary%2falpine%3apull&service=reg%2ehst%2eio'
    assert.is_not_nil(M.parse_bearer_data(h_auth))
    assert.same(exp, f(h_auth))
  end)

  it("bearer_scope2path", function()
    local h_auth = { -- from M.parse_auth_header
      name = 'Bearer',
      realm = 'https://auth.hst.io/token',
      service = 'registry.hst.io',
      scope = 'repo:library/alpine:pull',
    }
    assert.same('repo_library_alpine_pull', M.bearer_scope2path(h_auth, { '' }))
  end)

  it("get_authcache_dir", function()
    assert.same(os.getenv('HOME') .. '/.cache/www-auth', M.get_authcache_dir())
    M.set_auth_cache_dir('/tmp/auth')
    assert.same('/tmp/auth', M.get_authcache_dir())
  end)

  it("parse_string_data", function()
    local f = M.parse_string_data
    assert.same(1705929651, f("2024-01-22T16:20:51.86663282Z"))
  end)

  it("parse_data_header", function()
    local line = 'Mon, 22 Jan 2024 17:14:44 GMT'
    assert.same(1705932884, M.parse_data_header(line))
  end)

  it("parse_bearer_data", function()
    local hauth = {
      name = 'Bearer',
      realm = 'https://auth.host.io/token',
      service = 'registry.host.io',
      scope = 'repository:library/alpine:pull',
    }
    local exp = {
      name = 'Bearer',
      realm = 'https://auth.host.io/token',
      service = 'registry.host.io',
      scope = 'repository:library/alpine:pull',
      realm_url = {
        scheme = 'https',
        host = 'auth.host.io',
        path = '/token',
      }
    }
    assert.same(exp, M.parse_bearer_data(hauth))
  end)

  -- json store token to auth
  it("get_cached_bearer_json (alive and expired)", function()
    local realm = 'auth.host.io'
    local hauth = {
      name = 'Bearer',
      realm = 'https://auth.host.io/token',
      service = 'registry.host.io',
      scope = 'repository:library/alpine:pull',
    }
    -- log.fast_setup()
    local cache_dir = uv.cwd() .. '/spec/resources/auth/'
    local relpath = M.bearer_scope2path(hauth, {})
    assert.same('repository_library_alpine_pull', relpath)

    M.set_auth_cache_dir(cache_dir)
    -- do not remove expited date(from test-resources)
    -- ttl of auth data is 5min
    local now = M.parse_string_data('2024-01-22T18:29:51.0Z') + 60

    local json = M.get_cached_bearer_json(realm, relpath, now)
    assert.is_string(json) -- cached json alive - not expired

    now = now + 1000       -- ensure expired time
    assert.is_nil(M.get_cached_bearer_json(realm, relpath, now))
  end)
end)
