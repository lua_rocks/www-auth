## www-auth

Experimental luarock package to work with www-authentification

## Installation

To install the latest release

```sh
git clone --depth 1 https://gitlab.com/lua_rocks/www-auth
cd alogger
luarocks make
```


## Requirements

* [Lua] 5.1+ **or** [LuaJIT] 2.0+
* alogger

